/**
 * Marlin 3D Printer Firmware
 * Copyright (c) 2020 MarlinFirmware [https://github.com/MarlinFirmware/Marlin]
 *
 * Based on Sprinter and grbl.
 * Copyright (c) 2011 Camiel Gubbels / Erik van der Zalm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include "env_validate.h"

//无板载USB
#define BOARD_NO_NATIVE_USB

#define BOARD_WEBSITE_URL "https://oshwhub.com/jusha/rabbit-3d-printer"

//#define DISABLE_DEBUG
#define DISABLE_JTAG
/**
 * RABBIT 3D PRINTER (STM32F103RCT6) board pin assignments
 */

#if HOTENDS > 1 || E_STEPPERS > 1
  #error "Rabbit 3D Printer only supports one hotend / E-stepper. Comment out this line to continue."
#endif

#ifndef BOARD_INFO_NAME
  #define BOARD_INFO_NAME "Rabbit 3D Printer"
#endif

//
// Steppers
//
#ifndef X_CS_PIN
  #define X_CS_PIN                          PD2
#endif
#ifndef Y_CS_PIN
  #define Y_CS_PIN                          PC12
#endif
#ifndef Z_CS_PIN
  #define Z_CS_PIN                          PC11
#endif
#ifndef E0_CS_PIN
  #define E0_CS_PIN                         PC7
#endif

//
// Software SPI pins for TMC2130 stepper drivers
//
#if ENABLED(TMC_USE_SW_SPI)
  #ifndef TMC_SW_MOSI
    #define TMC_SW_MOSI                     PB15
  #endif
  #ifndef TMC_SW_MISO
    #define TMC_SW_MISO                     PB14
  #endif
  #ifndef TMC_SW_SCK
    #define TMC_SW_SCK                      PB13
  #endif
#endif

//
// EEPROM
//
#if EITHER(NO_EEPROM_SELECTED, FLASH_EEPROM_EMULATION)
  #define FLASH_EEPROM_EMULATION
  #define EEPROM_PAGE_SIZE     (0x800U)           // 2KB
  //最后的2K空间存放参数
  #define EEPROM_START_ADDRESS (0x8000000UL + (STM32_FLASH_SIZE) * 1024UL - (EEPROM_PAGE_SIZE) * 2UL)
  #define MARLIN_EEPROM_SIZE    EEPROM_PAGE_SIZE  // 2KB
#endif

//
// Servos BL_TOUCH
//
#define SERVO0_PIN                          PA3

//
// Limit Switches
//
#define X_STOP_PIN                          PB10
#define Y_STOP_PIN                          PB11
#define Z_STOP_PIN                          PB12
#define Z_MAX_PIN                           PC6

//
// Steppers
//
#define X_STEP_PIN                          PB5
#define X_DIR_PIN                           PB2
#define X_ENABLE_PIN                        PB4

#define Y_STEP_PIN                          PC10
#define Y_DIR_PIN                           PA15
#define Y_ENABLE_PIN                        PB3

#ifndef Z_STEP_PIN
  #define Z_STEP_PIN                        PB7
#endif
#ifndef Z_DIR_PIN
  #define Z_DIR_PIN                         PB8
#endif
#define Z_ENABLE_PIN                        PB6

#define E0_STEP_PIN                         PA12
#define E0_DIR_PIN                          PA11
#define E0_ENABLE_PIN                       PB9

#if HAS_TMC_UART
  /**
   * TMC2208/TMC2209 stepper drivers
   *
   * Hardware serial communication ports.
   * If undefined software serial is used according to the pins below
   */
  //#define X_HARDWARE_SERIAL  MSerial1
  //#define Y_HARDWARE_SERIAL  MSerial1
  //#define Z_HARDWARE_SERIAL  MSerial1
  //#define E0_HARDWARE_SERIAL MSerial1

  //
  // Software serial
  //
  #define X_SERIAL_TX_PIN                   PD2
  #define X_SERIAL_RX_PIN                   PD2

  #define Y_SERIAL_TX_PIN                   PC12
  #define Y_SERIAL_RX_PIN                   PC12

  #define Z_SERIAL_TX_PIN                   PC11
  #define Z_SERIAL_RX_PIN                   PC11

  #define E0_SERIAL_TX_PIN                  PC7
  #define E0_SERIAL_RX_PIN                  PC7

  // Reduce baud rate to improve software serial reliability
  #define TMC_BAUD_RATE                    19200
#endif

//
// Heaters 0,1 / Fans / Bed
//
#define HEATER_0_PIN                        PC9
#define FAN_PIN                             PA8
#define HEATER_BED_PIN                      PC8

//
// Temperature Sensors
//
#define TEMP_BED_PIN                        PA1   // TB
#define TEMP_0_PIN                          PA0   // TH1

//断料检测
#define FIL_RUNOUT_PIN                      PC13  // MT_DET

#define BEEPER_PIN                        PC1



//EXP GPIO定义
/**
 *                _____                                      _____                                     _____
 *  (BEEPER) PC1 | 1 2 | PC3 (BTN_ENC)          (MISO) PB14 | 1 2 | PB13 (SD_SCK)                  5V | 1 2 | GND
 *  (LCD_EN) PA4 | 3 4 | PA5 (LCD_RS)        (BTN_EN1)  PB1 | 3 4 | PA15 (SD_SS)         (LCD_EN) PA4 | 3 4 | PA5  (LCD_RS)
 *  (LCD_D4) PA6 | 5 6   PA7 (LCD_D5)        (BTN_EN2)  PB0 | 5 6   PB15 (SD_MOSI)       (LCD_D4) PA6 | 5 6   PB0  (BTN_EN2)
 *  (LCD_D6) PC4 | 7 8 | PC5 (LCD_D7)      (SD_DETECT)  PC0 | 7 8 | RESET                       RESET | 7 8 | PB1  (BTN_EN1)
 *           GND | 9 10| 5V                             GND | 9 10| 5V                  (BTN_ENC) PC3 | 9 10| PC1  (BEEPER)
 *                -----                                      -----                                     -----
 *                EXP1                                       EXP2                                      EXP3
 */

#define EXP_BEEPER    PC1
#define EXP_LCD_EN    PA4
#define EXP_LCD_D4    PA6
#define EXP_LCD_D5    PA7
#define EXP_LCD_D6    PC4
#define EXP_LCD_D7    PC5
#define EXP_BTN_ENC   PC3 
#define EXP_LCD_RS    PA5
#define EXP_SD_MISO   PB14
#define EXP_BTN_EN1   PB1
#define EXP_BTN_EN2   PB0
#define EXP_SD_DETECT PC0
#define EXP_SD_SCK    PB13
#define EXP_SD_SS     PC2
#define EXP_SD_MOSI   PB15


#if HAS_WIRED_LCD
  #define LCD_PINS_ENABLE                   EXP_LCD_EN
  #define LCD_PINS_RS                       EXP_LCD_RS
  #define LCD_PINS_D4                       EXP_LCD_D4
  #define LCD_PINS_D5                       EXP_LCD_D5
  #define LCD_PINS_D6                       EXP_LCD_D6
  #define LCD_PINS_D7                       EXP_LCD_D7
  #define BTN_ENC                           EXP_BTN_ENC
  #define BTN_EN1                           EXP_BTN_EN1
  #define BTN_EN2                           EXP_BTN_EN2

  // MKS MINI12864 and MKS LCD12864B; If using MKS LCD12864A (Need to remove RPK2 resistor)
  #if ENABLED(MKS_MINI_12864)

    #define DOGLCD_CS                       EXP_LCD_EN
    #define LCD_RESET_PIN                   EXP_LCD_RS
    #define DOGLCD_A0                       EXP_LCD_D4
    #define LCD_BACKLIGHT_PIN               -1

    #define DOGLCD_SCK                      EXP_LCD_D5
    #define DOGLCD_MOSI                     EXP_LCD_D6
    #define FORCE_SOFT_SPI
    #define SOFTWARE_SPI

  #elif ENABLED(MKS_MINI_12864_V3)
  
    #define DOGLCD_CS                       EXP_LCD_EN
    #define LCD_RESET_PIN                   EXP_LCD_RS
    #define LCD_PINS_DC                     EXP_LCD_D4
    #define DOGLCD_A0                       LCD_PINS_DC
    #define LCD_BACKLIGHT_PIN               -1

    #define NEOPIXEL_PIN                    EXP_LCD_D7
    #define DOGLCD_SCK                      EXP_LCD_D5
    #define DOGLCD_MOSI                     EXP_LCD_D6
 
    #define FORCE_SOFT_SPI
    #define SOFTWARE_SPI
	//#define LCD_SCREEN_ROT_180

  #elif ENABLED(FYSETC_MINI_12864)
    #define DOGLCD_CS              EXP_LCD_EN
    #define DOGLCD_A0              EXP_LCD_D4
    #define DOGLCD_SCK             EXP_LCD_D5
    #define DOGLCD_MOSI            EXP_LCD_D6
    #define LCD_BACKLIGHT_PIN      EXP_LCD_D7
    #define FORCE_SOFT_SPI                      // Use this if default of hardware SPI causes display problems
                                                //   results in LCD soft SPI mode 3, SD soft SPI mode 0
    #define LCD_RESET_PIN          EXP_LCD_RS  // Must be high or open for LCD to operate normally.
  #endif 

#endif // HAS_WIRED_LCD


//
// SPI
//
// TODO: This is the only way to set SPI for SD on STM32 (for now)
#define ENABLE_SPI2

#define SPI_DEVICE                             2
#define SD_SCK_PIN                          EXP_SD_SCK
#define SD_MISO_PIN                         EXP_SD_MISO
#define SD_MOSI_PIN                         EXP_SD_MOSI
#define SD_SS_PIN                           EXP_SD_SS

//
// SD Card
//
#define ONBOARD_SPI_DEVICE                     2
#define SDSS                           SD_SS_PIN
#define SDCARD_CONNECTION              ONBOARD
#define SD_DETECT_PIN                  EXP_SD_DETECT
#define ONBOARD_SD_CS_PIN              SD_SS_PIN
#define NO_SD_HOST_DRIVE


